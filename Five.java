/**
 * Write a program to accept a line and check how many
 * consonants and vowels are there in line.
 */

import java.util.*;
class Five {
    public static void main(String args[]) {
        Scanner sr = new Scanner(System.in);
        System.out.println("Enter the string: ");
        String input = sr.nextLine();
        int i = 0, vovels = 0, constants = 0;
        String auxInput = input.toLowerCase();
        while(i < auxInput.length()) {
            char c = auxInput.charAt(i);
            if(c == 'a' || c == 'e' || c == 'o' || c == 'i' || c == 'u')
                vovels++;
            else if(!(c == ' ')) 
                constants++;
            i++;
        }
        System.out.println("After analyzed \'"+input+"\'");
        System.out.println("Vovels: "+vovels+" | Constants: "+constants);
    }
}

/**
 * Expected output:
 * 
 * Enter the string:
 * i am bhautik
 * After analyzed 'i am bhautik'
 * Vovels: 5 | Constants: 5
 * 
 */