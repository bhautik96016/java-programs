/**
 * 
 * @author Bhautik Chudasama
 * @since July 15, 2019 at 07:00 PM
 * Time Complaxity: O(n^2)
 * Space Complaxity: O(1)
 * 
 */

import java.util.*;

class InsertionSort {
    /**
     * 
     * @param array array of unsorted integers
     * @return return an array which Is sorted order
     */
    public static int[] insertionSort(int array[]) {
        int noOfSwappings = 1;
        int noOfComparisions = 1;
        for(int i=1; i<array.length; i++) { 
            int j = i; 
            noOfComparisions+=1;
            while(j > 0 && array[j] < array[j-1]) { 
                noOfSwappings+=1;
                int temp = array[j-1];
                array[j-1] = array[j];
                array[j] = temp;
                j-=1;
            }
        }
        System.out.println("No of Comparisions: " + noOfComparisions);
        System.out.println("No of Swappings: " + noOfSwappings);
        return array; 
    }

    public static void main(String[] args) {
        int array[] = new int[10000];
        int val = 9999;
        for(int i=0; i<9999; i++)
            array[i] = val--;

        long startingTime = System.nanoTime();
        int sa[] = insertionSort(array);
        long endingTime = System.nanoTime();
        float totalTime = (float)(endingTime - startingTime)/1000000;
        int i = 0;
        while(i < sa.length) {
            System.out.print(sa[i]+", ");
            i++;
        }
        System.out.println("\nTotal execution time of sorting is: " + totalTime + "ms");
    }
}