
/*
Create a class called Student. Write a student manager
program to manipulate the student information from
files by using FileInputStream and FileOutputStream.
*/

/*
    NOTE
    ~~~~
    student.txt file format

    =======================
    ENROLLMENT NO: long
    NAME: String
    SEMESTER: int
    CGPA: float
    DEPARTMENT: String
    =======================
*/

import java.util.*;
import java.io.*;

class Student {
    String name = "", department = "";
    int sem = 0;
    long eid;
    float CGPA;
    Student() {
        File studentDataFile = new File("/", "student.txt");
        try {
            if(!studentDataFile.exists()) {
            // file is not exists
                if(!studentDataFile.createNewFile()) {
                    // file is not created due to memory or other issues.
                    System.out.println("Something went wrong!!");
                }
            }
        }
        catch(IOException e) {
            System.out.println(e);
        }
    }
    void manipulateStudentData() {
        Scanner sr = new Scanner(System.in);
    }
    void readAndTransform(int i, int ch) {
        switch (i) {
            case 0:
                this.eid = Long.parseLong(String.valueOf(this.eid)+(char)ch);          
                break;
            case 1:
                this.name = this.name + (char)ch;
                break;
            case 2:
                this.sem = Integer.parseInt(String.valueOf((char)ch));
                break;
            case 3:
              //  this.CGPA = Float.parseFloat(String.valueOf(this.CGPA) + (char)ch);
                break;
            case 4:
              //  this.department = this.department + (char) ch;
                break;
            default:
                break;
        }
    }

    void displayStudentData() {
        try {
            FileInputStream fi = new FileInputStream("student.txt");
            int totalAvailableBytes = fi.available();
            if(totalAvailableBytes > 0) {
                int ch, i=0;
                while((ch=fi.read())!=-1) {
                    if(ch == 13) {
                        i++;
                    }
                    else {
                        readAndTransform(i, ch);
                    }
                }
                System.out.println("============================");
                System.out.println("Enrollment number: " + this.eid);
                System.out.println("Name: " + this.name);
                System.out.println("SEMESTER: " + this.sem);
                System.out.println("CGPA: " + this.CGPA);
                System.out.println("DEPARTMENT: " + this.department);
                System.out.println("============================");
            }
            else {
                throw new IOException("Empty file!");
            }
        }
        catch(IOException e) {
            System.out.println("Error: "+e);
        }       
    }
}

class Eleven extends Student {
    public static void main(String[] args) {
        Student s = new Student();
        s.displayStudentData();
        
    }
}