import java.beans.Encoder;
import java.util.*;
import java.io.*;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import javax.crypto.spec.SecretKeySpec;

import org.json.*;
/**
 * @author Bhautik Chudasama
 * @version 1.0.0
 */
class Auth {

    Boolean authStatus = false;
    Scanner sr = new Scanner(System.in); 
    enum Privilages {
        ALLOW_READ,
        ALLOW_WRITE,
        ALLOW_CREATE_USER
    }
    Auth() {
        File authenticationFile = new File("/", "auth.json");
        File studentFile = new File("/", "student.json");

        if(authenticationFile.exists()) {

        }
        else {
            try {
                authenticationFile.createNewFile();
                authenticationDailog(true);
            }
            catch(Exception e) {
                System.out.println("Error! File cannot create!");
                System.exit(1);
            }
        }
        if(studentFile.exists()) {

        }
        else {
            try {
                studentFile.createNewFile();
            }
            catch(Exception e) {
                System.out.println("Error! File cannot create!");
                System.exit(1);
            }
            
        }
    }
    

    Boolean authenticationDailog(Boolean userType) {
        String userEmail = "";
        String userPassword = "";
        if(userType) {
            System.out.println("Enter the new email: ");
            userEmail = sr.nextLine();
            System.out.println("Enter the new password: ");
            userPassword = sr.nextLine();
           

            return true;
        }
        else {
            System.out.println("Enter the email: ");
            userEmail = sr.nextLine();
            System.out.println("Enter the password: ");
            userPassword = sr.nextLine();
            return true;
        }    
    }
    
    /**
     * @param userEmail The email of user you want to change the rights.
     * @param privilage For ALLOW_READ = 0, ALLOW_WRITE = 0, ALLOW_CREATE_USER = 0
     * @return Return TRUE if success otherwise false.
     */
    Boolean changePrivilages(String userEmail, Privilages rights) {
        if(authStatus) {
            return true;
        }
        else {
            return false;
        }
    }
    Boolean b = changePrivilages("b@gmao.c", Privilages.ALLOW_CREATE_USER);
}



class ElevenAux {
    public static void main(String[] args) {
        try {
            Scanner sr = new Scanner(System.in);
            String email = "";
            String  password = "";
            Boolean isAdmin = false, isLoggedIn = false;
            System.out.println("Verifing...");
            /*
            if(isAdmin) {
                // Already admin is created..
                System.out.println("------------------------");
                System.out.println("     STUDENT MANAGER    ");
                System.out.println("------------------------");
                System.out.println("Enter the email:");
                email = sr.nextLine();
                System.out.println("Enter the password:");
                password = sr.nextLine();
            }
            else {
                System.out.println("------------------------");
                System.out.println("     STUDENT MANAGER    ");
                System.out.println("------------------------");
                System.out.println("Enter the email:");
                email = sr.nextLine();
                System.out.println("Enter the password:");
                password = sr.nextLine();
            }
            System.out.println("Verifing...");
            Map<String, Object> docData = new HashMap<>();
            */
            DESKeySpec keySpec = new DESKeySpec("admin@java.org".getBytes("UTF8"));
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey key = keyFactory.generateSecret(keySpec);
            String p = "bhautik";
            byte[] clearText = p.getBytes("UTF8"); 
            Cipher c = Cipher.getInstance("DES");
            c.init(Cipher.ENCRYPT_MODE, key);
            byte[] encrypedPwdBytes = c.doFinal(clearText);
            
            String encryptedPassword = new String(encrypedPwdBytes);

            
            Cipher cipher = Cipher.getInstance("DES");// cipher is not thread safe
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] plainTextPwdBytes = (cipher.doFinal(encrypedPwdBytes));
            System.out.println(new String(plainTextPwdBytes));
        }
        catch(Exception e) {

        }
    }
}