// Copyright 2019 Bhautik
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.



import java.util.*;
/**
 * @author Bhautik Chudasama
 * @version 1.0.0 In which Time complaxity Is O(N*logn) and Space complacity Is O(n).
 */
public class MergeSortOriginal {
    public static int doMergeCalls = 0;
    public static int[] mergeSort(int a[]) {
        if(a.length <= 1)
            return a;
        int auxillaryArray[] = Arrays.copyOfRange(a, 0, a.length);
        mergeSortHelper(a, 0, a.length-1, auxillaryArray);
        System.out.println("Main | Aux");
        return a;
    }

    public static void mergeSortHelper(int a[], int startingIndex, int endingIndex, int aux[]) {
        System.out.println("StartingIndex: "+startingIndex+ " | EndingIndex: "+endingIndex);
        if(startingIndex == endingIndex) {
            System.out.println("Return at StartingIndex: " + startingIndex + "| EndingIndex: " + endingIndex);
            return;
        }
        int middleIndex = (int)Math.floor((startingIndex+endingIndex)/2);
        System.out.println("MiddleIndex: "+middleIndex);
        
        mergeSortHelper(aux, startingIndex, middleIndex, a);
        mergeSortHelper(aux, (middleIndex+1), endingIndex, a);
        doMerge(a, startingIndex, middleIndex, endingIndex, aux);
    }
    public static void doMerge(int a[], int startingIndex, int middleIndex, int endingIndex, int aux[]) {
        doMergeCalls+=1;
        int i = startingIndex;
        int j = middleIndex + 1;
        int k = startingIndex;
        while(i <= middleIndex && j <= endingIndex) {
            if(aux[i] <= aux[j]) {
                a[k] = aux[i];
                i+=1;
            }
            else {
                a[k] = aux[j];
                j+=1;
            }
            k+=1;
        }
        while(i <= middleIndex) {
            a[k] = aux[i];
            i+=1;
            k+=1;
        }
        while(j <= endingIndex) {
            a[k] = aux[j];
            j+=1;
            k+=1;
        }
     }
    public static void main(String args[]) {
        int a[] = new int[10];
        int targetNo = 10;
        for(int i=0; i<10; i++)
            a[i] = targetNo--;
        long startingTime = System.nanoTime();
        int sortedArray[] = mergeSort(a);
        long endingTime = System.nanoTime();
        for (int i = 0; i < sortedArray.length; i++) {
            System.out.print(sortedArray[i] + ", ");
        }
        float totalTime = (float) (endingTime - startingTime) / 1000000;
        System.out.print("\n No of merge calls: " + doMergeCalls);
        System.out.println("\nTotal execution time of search is: " + totalTime + "ms");
    }
}

/*
Output:
*~*~*~*~*
StartingIndex: 0 | EndingIndex: 3
MiddleIndex: 1
StartingIndex: 0 | EndingIndex: 1
MiddleIndex: 0
StartingIndex: 0 | EndingIndex: 0
Return at StartingIndex: 0| EndingIndex: 0
StartingIndex: 1 | EndingIndex: 1
Return at StartingIndex: 1| EndingIndex: 1
StartingIndex: 2 | EndingIndex: 3
MiddleIndex: 2
StartingIndex: 2 | EndingIndex: 2
Return at StartingIndex: 2| EndingIndex: 2
StartingIndex: 3 | EndingIndex: 3
Return at StartingIndex: 3| EndingIndex: 3
2, 3, 4, 5
 */