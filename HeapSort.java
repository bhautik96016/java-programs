/**
 * 
 * @author Bhautik Chudasama
 * @since July 27, 2020 at 06:25 PM
 * Time complaxity: O(n);
 * Space complaxity: O(1);
 * 
 */

import java.util.*;

class HeapSort {
  
    /**
     * @param array array of integer numbers
     * @return The new sorted array
     */
    public static int[] heapSort(int array[]) {
        buildMaxHeap(array);
        for(int i = array.length-1; i >= 0; i--) {
            // Already builded max heap.
            int temp = array[0];
            array[0] = array[i];
            array[i] = temp;
            // Continuesly swap the gratest value called ROOT
            shiftDown(0, i-1, array);
        }
        return array;
    }
    /**
     * @param array array of intereger numbers
     */
    public static void buildMaxHeap(int array[]) {
        int firstParentIndex = (int)Math.floor(array.length-1 / 2);
        for(int currentIndex = firstParentIndex+1; currentIndex >= 0; currentIndex--) {
            shiftDown(currentIndex, array.length-1, array);
        }
    }
    /**
     * @param currentIndex current index of array
     * @param endIndex end index of array
     * @param array array of remaing unsorted array
     */
    public static void shiftDown(int currentIndex, int endIndex, int array[]) {
        int childOneIndex = ((currentIndex * 2) + 1);
        int childTwoIndex;
        int indexToSwap;
        while(childOneIndex <= endIndex) {

            if((currentIndex * 2) + 2 <= endIndex)
                childTwoIndex = (currentIndex * 2) + 2;
            else 
                childTwoIndex = -1;

            if(childTwoIndex > -1 && array[childTwoIndex] > array[childOneIndex])
                indexToSwap = childTwoIndex;
            else 
                indexToSwap = childOneIndex;

            if(array[indexToSwap] > array[currentIndex]) {
                int temp = array[currentIndex];
                array[currentIndex] = array[indexToSwap];
                array[indexToSwap] = temp;

                currentIndex = indexToSwap;
                childOneIndex = (currentIndex * 2) + 1;
            }
            else 
                return;
        }
    }

  public static void main(String[] args) {
        int array[] = {10, 0, 5, 2, 4, 7, 15, 14, 41, 78, 17, 4};
        long startingTime = System.nanoTime();
        int sa[] = heapSort(array);
        long endingTime = System.nanoTime();
        float totalTime = (float)(endingTime - startingTime)/100000000;
        int i = 0;
        while(i < sa.length) {
            System.out.print(sa[i]+", ");
            i++;
        }
        System.out.println("\nTotal execution time of sorting is: "+totalTime+"s");
  }
}

/*
 * OUTPUT: 
 * > javac HeapSort.java
 * > java HeapSort
 * 0, 2, 4, 4, 5, 7, 10, 14, 15, 17, 41, 78,
 * Total execution time of sorting is: 0.00255615s
 */