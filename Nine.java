
/*
Write an interactive program to print a string entered in
a pyramid form. For instance, the string
“stream” has to be displayed as follows:

      S 
     S t 
    S t r 
   S t r e 
  S t r e a 
 S t r e a m
*/

import java.util.*;
class Nine {
    public static void main(String args[]) {
        String s = "Stream";
        for(int i=0; i < s.length(); i++) {
            for(int j=s.length(); j>i; j--) {
                System.out.print(" ");
            }
            for(int j=0; j<=i; j++) {
                System.out.print(s.charAt(j)+" ");
            }
            System.out.println("");
        }
    }
}

/**
 * Expected output:
      S 
     S t 
    S t r 
   S t r e 
  S t r e a 
 S t r e a m

 */