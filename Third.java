/**
 * Write a program to enter two numbers and perform mathematical operations on
 * them.
 */

 import java.util.*;
 
 class Third {
     public static void main(String args[]) {
        int a = 5, b = 8;
        System.out.println("Basic operations performed on A and B");
        System.out.println("A: "+a);
        System.out.println("B: "+b);
        System.out.println("Addition: "+(a+b));
        System.out.println("Subtraction: "+(a-b));
        System.out.println("Multiplication: "+(a*b));
        System.out.println("Division: "+(a/b));
        System.out.println("Modulo: "+(a%b));
     }
 }

 /**
  * Expected output:
  * -----------------------------
  * Basic operations performed on A and B
  * A: 5
  * B: 8
  * Addition: 13
  * Subtraction: -3
  * Multiplication: 40
  * Division: 0
  * Modulo: 5
  */