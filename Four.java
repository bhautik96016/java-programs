
/**
 * Write a program to find length of string and print
 * second half of the string.
 */

import java.util.*;

class Four {
    public static void main(String args[]) {
        String s = "BhautikChudasama";
        int n = 0, cc = 0;
        // find length using built in array.
        n = s.length();
        // find the length using without using built-in method. 
        char c[] = s.toCharArray();
        for(int i=0; i < c.length; i++) {
            cc++;
        }
        System.out.println("Length of string is: "+ cc);
        for(int i=0; i<n/2; i++) {
            System.out.print(s.charAt(i));
        }
    }
}

/**
 * Expected output: 
 * Length of string is: 16 
 * BhautikC
 */