/**
 * Write a program to find that given number or string is
 * palindrome or not.
 */
 
 import java.util.*;

 class Seven {
    public static void main(String args[]) {
        Scanner sr = new Scanner(System.in);
        System.out.print("Enter the string: ");
        String input = sr.nextLine();
        input = input.toLowerCase();
        if(!input.isEmpty()) {
            int l = input.length()-1;
            int i = 0;
            int possibility = -1;
            while(l >= 0) {
                if(input.charAt(l) == input.charAt(i)) {
                    possibility++;   
                }
                i++;
                l--;
            }
            if(possibility == input.length()-1)
                System.out.println("String Is Palindrom!!");
            else 
                System.out.println("String Is not Palindrom!!");
        }
        else 
            System.out.print("Enter the valid string!");
    }
 }

 /**
  * Expected output:
  * Enter the string: Oyo
  * String Is Palindrom!!
  */