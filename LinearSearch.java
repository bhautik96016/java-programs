/**
 * 
 * @author Bhautik Chudasama
 * @since July 27, 2017 at 06:37 PM
 * 
 */

 import java.util.*;

 class LinearSearch {

    public static int linearSearch(int array[], int key) {
        int i=0;
        while(i < array.length) {
            if(array[i] == key) 
                return i;
            i+=1;
        }
        return -1;
    }
    public static void main(String[] args) {
        int array[] = new int[999];
        int limit = 999;
        int pos = -1;
        for(int i=0; i<999; i++) 
            array[i] = limit--;
        long startingTime = System.nanoTime();
        pos = linearSearch(array, 0);
        long endingTime = System.nanoTime();
        float totalTime = (float)(endingTime - startingTime)/1000000;
        if(pos == -1)
            System.out.println("Element not found!");
        else 
            System.out.println("Element found at "+pos+" index!");
        System.out.println("\nTotal execution time of search is: " + totalTime + "ms");
    }
 }