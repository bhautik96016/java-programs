/*
 * Write a program to convert rupees to dollar. 60
 * rupees=1 dollar.
 */

 import java.text.DecimalFormat;
 import java.util.*;
 class First {
    public static void main(String[] args) {
        float ruppes = 0, dollars = 0;
        System.out.print("Enter the ruppes: ");
        Scanner sr = new Scanner(System.in);
        ruppes = sr.nextFloat();
        if(ruppes < 0) {
            System.out.println("Enter the valid ruppes for convert into the dollars!");
        }
        else {
            if(ruppes == 0) {
                dollars = 0;
            }
            else {
                dollars = ruppes / 60;
            }
            System.out.println("Dollars: $"+ new DecimalFormat("0.00").format(dollars));
        }
    }
 }

 /*
 * Expected output:
 * ---------------------
 * Enter the ruppes: 5
 * Dollars: $0.08
 * ---------------------
 */