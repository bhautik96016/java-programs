/**
 * 
 * @author Bhautik Chudasama
 * @since July 27, 2019 at 06:11 PM
 * Time Complaxity: O(n^2)
 * Space Complaxity: O(n^2)
 * 
 */

 import java.util.*;

 class SelectionSort {
    /**
     * 
     * @param array array of unsorted integer numbers
     * @return array of sorted integer numbers
     */
    public static int[] selectionSort(int array[]) {
        int noOfComparisions = 1;
        int noOfSwappings = 1;
        int startingIndex = 0;
        while(startingIndex < array.length-1) {
            int smallestIndex = startingIndex;
            for(int i=startingIndex; i<array.length; i++) {
                noOfComparisions+=1;
                if(array[i] < array[smallestIndex]) {
                    smallestIndex = i;
                }
            }
            noOfSwappings+=1;
            int temp = array[startingIndex];
            array[startingIndex] = array[smallestIndex];
            array[smallestIndex] = temp;
            startingIndex+=1;
        }
        System.out.println("No of Comparisions: " + noOfComparisions);
        System.out.println("No of Swappings: " + noOfSwappings);
        return array;
    }
    public static void main(String[] args) {
        int array[] = new int[10000];
        int val = 9999;
        for (int i = 0; i < 9999; i++)
            array[i] = val--;
        long startingTime = System.nanoTime();
        int sa[] = selectionSort(array);
        long endingTime = System.nanoTime();
        float totalTime = (float) (endingTime - startingTime) / 1000000;
        int i = 0;
        /*
        while (i < sa.length) { 
            System.out.print(sa[i] + ", "); 
            i++; 
        }
        */
        System.out.println("\nTotal execution time of sorting is: " + totalTime + "ms");
    }
 }