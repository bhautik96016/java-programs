/*
    Write an interactive program to print a diamond shape.
    For example, if user enters the number 3, the diamond
    will be as follows:

      *
     * * 
    * * *
     * *
      * 

*/

import java.util.*;

class Ten {
    public static void main(String args[]) {
        Scanner sr = new Scanner(System.in);
        System.out.print("Enter the number: ");
        int n = sr.nextInt();
        for(int i=0; i<n; i++) {
            for(int j=n; j>i; j--) {
                System.out.print(" ");
            }
            for(int j=0; j<=i; j++) {
                System.out.print("* ");
            }
            System.out.println("");
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < i+2; j++) {
                System.out.print(" ");
            }
            for (int j = n-1; j > i; j--) {
                System.out.print("* ");
            }
            System.out.println("");
        }
    }
}