/**
 * Write a program that calculates percentage marks of the student if marks of 6
 * subjects are given.
 */

 import java.util.*;
 import java.text.DecimalFormat;

 class Second {
     public static void main(String[] args) {
        int a[], n, total = 0;
        float percentage = 0;    
        Scanner sr = new Scanner(System.in);
        try {            
            a = new int[6];
            for(int i=0; i<a.length; i++) {
                System.out.print("Enter the subject "+(i+1)+" marks: ");
                a[i] = sr.nextInt();
            }
            for(int i=0; i<a.length; i++) {
                if(a[i]>=0) {
                    total+=a[i];
                }
                else {
                    throw new Exception("Please enter the valid marks of subject "+(i++));
                }
            }
            percentage = total / a.length;
            System.out.println("Your percentage: "+(new DecimalFormat("0.00").format(percentage)));
        }
        catch(Exception e) {
            System.out.println(e);
        }
     }
 }

 /**
 * Expected output: 
 * ------------------------------------ 
 * Enter the subject 1 marks: 80 
 * Enter the subject 2 marks: 90 
 * Enter the subject 3 marks: 78 
 * Enter the subject 4 marks: 90 
 * Enter the subject 5 marks: 67 
 * Enter the subject 6 marks: 89 
 * Your percentage: 82.00
 * ------------------------------------
 * Enter the subject 1 marks: 89
 * Enter the subject 2 marks: 7
 * Enter the subject 3 marks: 0
 * Enter the subject 4 marks: -5
 * Enter the subject 5 marks: 90
 * Enter the subject 6 marks: 77
 * java.lang.Exception: Please enter the valid marks of subject 3
 * ------------------------------------
 */