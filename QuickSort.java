import java.util.*;

/**
 * 
 * @author Bhautik Chudasama
 * Time Complaxity: O(n*logn);
 * Space Complaxity: O(n);
 * 
 */
class QuickSort {
    /**
     * 
     * @param array unsorted integer array
     * @return sorted int array
     */
    public static int noOfPartitions = 0;

    public static int[] quickSort(int array[]) {
        quickSortHelper(array, 0, array.length-1);
        return array;
    }
    /**
     * 
     * @param array array of element
     * @param startingIndex starting where you want to start
     * @param endingIndex ending where you want to end
     */
    public static void quickSortHelper(int array[], int startingIndex, int endingIndex) {
        noOfPartitions+=1;
        if(startingIndex >= endingIndex)
            return;
        int pivot = startingIndex;
        int leftIndex = startingIndex+1;
        int rightIndex = endingIndex;
        while(rightIndex >= leftIndex) {
            if(array[leftIndex]>array[pivot] && array[rightIndex]<array[pivot]) {
                int temp = array[leftIndex];
                array[leftIndex] = array[rightIndex];
                array[rightIndex] = temp;
            }
            if(array[leftIndex] <= array[pivot]) {
                leftIndex+=1;
            }
            else {
                rightIndex-=1;
            }
        }
        int temp = array[pivot];
        array[pivot] = array[rightIndex];
        array[rightIndex] = temp;

        Boolean leftSubArray = rightIndex - 1 - startingIndex < endingIndex - (rightIndex + 1);
        if(leftSubArray) {
            quickSortHelper(array, startingIndex, rightIndex - 1);
            quickSortHelper(array, rightIndex+1, endingIndex);
        }
        else {
            quickSortHelper(array, rightIndex+1, endingIndex);
            quickSortHelper(array, startingIndex, rightIndex - 1);
        }
    }

    public static void main(String[] args) {
        int array[] = new int[30];
        int targetNo = 30;
        for(int i=0; i<30; i++)
            array[i] = targetNo--;
        long startingTime = System.nanoTime();
        int sa[] = quickSort(array);
        long endingTime = System.nanoTime();
        for (int i = 0; i < sa.length; i++) {
            System.out.print(array[i]+", ");
        }
        float totalTime = (float)(endingTime - startingTime)/1000000;
        System.out.print("\n No of partitions: "+noOfPartitions);
        System.out.println("\nTotal execution time of search is: " + totalTime + "ms");
        
    }
}