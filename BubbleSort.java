/**
 * 
 * @author Bhautik Chudasama
 * @since July 27, 2020 at 05:42 PM
 * Time Complaxity: O(n^2) Worst Case
 * Time Complaxity: O(n) Best Case
 * Space Complaxity: O(1)
 * 
 */

 import java.util.*;

 class BubbleSort {

    /**
     * 
     * @param array array of unsorted integer numbers
     * @return array of sorted integer numbers
     */
    public static int[] bubbleSort(int array[]) {
        boolean isSorted = false;
        int noOfComparisions = 1;
        int noOfSwappings = 1;
        while(!isSorted) {
            int counter = 1;
            isSorted = true;
            for(int i=0; i<array.length-counter; i++) {
                noOfComparisions+=1;
                if(array[i] > array[i+1]) {
                    noOfSwappings+=1;
                    int temp = array[i];
                    array[i] = array[i+1];
                    array[i+1] = temp;
                    isSorted = false;
                }
            }
            counter+=1;
        }
        System.out.println("No of Comparisions: " + noOfComparisions);
        System.out.println("No of Swappings: " + noOfSwappings);
        return array;
    }
    public static void main(String[] args) {
        int array[] = new int[10000];
        int val = 10000;
        for (int i = 0; i < 9999; i++)
            array[i] = val--;
        long startingTime = System.nanoTime();
        int sa[] = bubbleSort(array);
        long endingTime = System.nanoTime();
        float totalTime = (float)(endingTime - startingTime)/1000000;
        int i = 0;
        /*
        while (i < sa.length) {
            System.out.print(sa[i] + ", ");
            i++;
        }
        */
        System.out.println("\nTotal execution time of sorting is: " + totalTime + "ms");
    }
 }