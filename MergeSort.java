import java.util.*;

class MergeSort {
    public static int[] mergeSort(int a[]) {
        // if best case return array
        if(a.length == 1)
            return a;
        int middleIndex = (int)Math.floor(a.length/2);
        int leftHalf[] = Arrays.copyOfRange(a, 0, middleIndex); // 0 -> middleIndex-1
        int rightHalf[] = Arrays.copyOfRange(a, middleIndex, a.length); // middleIndex -> length-1
        return mergeSortArrays(mergeSort(leftHalf), mergeSort(rightHalf));
    }
    public static int[] mergeSortArrays(int leftHalf[], int rightHalf[]) {
        int sortedArray[] = new int[leftHalf.length + rightHalf.length];
        int i = 0, j = 0, k = 0;
        while(i < leftHalf.length && j < rightHalf.length) {
            if(leftHalf[i] < rightHalf[j]) {
                sortedArray[k] = leftHalf[i];
                i+=1;
                k+=1;
            }
            else  {
                sortedArray[k] = rightHalf[j];
                j+=1;
                k+=1;
            }
        }
        // For remaining elements
        while(i < leftHalf.length) {
            sortedArray[k] = leftHalf[i];
            i+=1;
            k+=1;
        }
        while(j < rightHalf.length) {
            sortedArray[k] = rightHalf[j];
            j+=1;
            k+=1;
        }
        return sortedArray;
    }
    public static void main(String args[]) {
        int a[] = {55, 22, 100, -10, 5};
        int sortedArray[] = mergeSort(a);
        for(int i=0; i < sortedArray.length; i++) {
            System.out.println(sortedArray[i]);
        } 
    }
}