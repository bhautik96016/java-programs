/**
 * Write a program to count the number of words that
 * start with capital letters.
 */

 import java.util.*;
 
 class Six {
     public static void main(String args[]) {
        Scanner sr = new Scanner(System.in);
        System.out.println("Enter the string: ");
        String input = sr.nextLine();
        // \ means starting with and \s+ means morethan one space.
        String arr[] = input.split("\\s+");
        int i = 0, count = 0;
        while(i < arr.length) {
            char f = arr[i].charAt(0);
            if(f >= 'A' && f <= 'Z')
                count++;
            i++;
        }
        System.out.println("Capitalize letter is: "+count);
     }
 }

