/*
* Create a class which ask the user to enter a sentence,
* and it should display count of each vowel type in the
* sentence. The program should continue till user enters
* a word “quit”. Display the total count of each vowel
* for all sentences.
*/

 import java.util.*;
 class InputSystem {
    Scanner sr = new Scanner(System.in);
    String input;
    InputSystem() {
        while (true) {
            System.out.println("Enter the string: ");
            input = sr.nextLine();
            int s = input.toLowerCase().compareTo("quit");
            if(s == 0) {
                System.out.println("System exited with status 1!");
                System.exit(1);
            }
            else {
                int vovels = 0;
                input = input.toLowerCase();
                int i = 0;
                while(i < input.length()) {
                    char c = input.charAt(i);
                    if(c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u')
                        vovels++;
                    i++;
                }
                System.out.println("Vovels: "+vovels);
            }
        }
    }

 }

 class Eight {
     public static void main(String args[]) {
        InputSystem i = new InputSystem();
     }
 }

 /**
  * Expected output:
  * Enter the string:
  * bhautik
  * Vovels: 3
  * Enter the string:
  * quit
  * System exited with status 1!
  */